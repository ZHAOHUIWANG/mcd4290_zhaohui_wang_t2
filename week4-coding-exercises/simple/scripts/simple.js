

function doIt(){
    let num1Ref = document.getElementById("number1")
    let num2Ref = document.getElementById("number2")
    let num3Ref = document.getElementById("number3")
    
    let outputArea = document.getElementById("answer")
    
    let evenOrOdd = document.getElementById("evenOrodd")
    
    let answer = Number(num1Ref.value) + Number(num2Ref.value) + Number(num3Ref.value)
    outputArea.innerText = answer
    
    if(answer > 0){
        outputArea.className = "positive"
    }else{
        outputArea.className = "negative"
    }
    

    let num_EorO = ""
    if(answer%2 == 0){
        num_EorO = "(Even)"
        evenOrOdd.className ="Even"
    }else{
        num_EorO = "(Odd)"
        evenOrOdd.className ="Odd"
    }
    
    evenOrOdd.innerText = num_EorO
}