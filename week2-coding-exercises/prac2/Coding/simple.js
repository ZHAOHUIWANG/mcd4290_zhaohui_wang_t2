//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here
    let number = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25];
    
    let PositiveOdd = [];
    let NegativeEven = [];
    
    for(let i=0;i<number.length;i++){
        if(number[i] > 0 && number[i]%2 !== 0){
            PositiveOdd.push(number[i]);
        }else if(number[i] < 0 && number[i]%2 == 0){
            NegativeEven.push(number[i]);
        }
    }
    
    output = "Positive Odd: " + PositiveOdd +"\n"+ "Negative Even: "+NegativeEven;
    
    let outPutArea1 = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea1.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here
    let one = 0;
    let two = 0;
    let three = 0;
    let four = 0;
    let five = 0;
    let six = 0;

for (let i=0;i<60000;i++){

    let dice = Math.floor(Math.random() * 6) + 1;

    if (dice == 1){
        one += 1;
    }
    else if(dice == 2){
        two += 1;
    }
    else if(dice == 3){
        three += 1;
    }
    else if(dice == 4){
        four += 1;
    }
    else if(dice == 5){
        five += 1;
    }
    else if(dice == 6){
        six += 1;
    }
}
    output = "Frequency of die rolls" + "\n" + "1: "+one + "\n" + "2: "+two + "\n" + "3: "+three +"\n" + "4: "+four +"\n" + "5: "+five +"\n" + "6: "+six;

    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here
    let frequencies = [0, 0, 0, 0, 0, 0, 0];
    
    for(i =0; i < 60000; i++){
        let rand = Math.floor((Math.random() * 6) + 1);
        frequencies[rand] += 1;
    }
    output += "Frequency of die rolls";
    output += "\n";
    for(let i = 1; i < frequencies.length; i++){
        output += i+":"+frequencies[i]+"\n"
    }
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    let dieRolls = {
      Frequencies: {
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
      },
      Total:60000,
      Exceptions:""
    }
    for(let i = 0; i < dieRolls.Total; i++){
        let rand = Math.floor((Math.random() * 6) + 1);
        dieRolls.Frequencies[rand] += 1;
    }
    
    output += "Frequency of die rolls";
    
    for(let number in dieRolls.Frequencies){
    output += "\n"
    output += number + ":"
    output += dieRolls.Frequencies[number]
    }
    
    for(let numb in dieRolls.Frequencies){
        if(Math.abs(dieRolls.Frequencies[numb] - 10000) > 10000*0.01){
            dieRolls.Exceptions += numb
            dieRolls.Exceptions += " "
        }
    }
  output += "\n"
  output += "Exceptions:"
  output += dieRolls.Exceptions
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here
    let person = {
      name: "Jane",
    income: 127050
}
    let tax = 0;
    
    if (person.income < 18200){
      tax = 0;
    }else if (person.income < 37000){
      tax = (person.income - 18200)*0.19;
    }else if (person.income < 90000){
      tax = (3572 + ((person.income - 37000)*0.325));
    }else if (person.income < 180000){
      tax = (20797 + ((person.income - 90000)*0.37));
    }else{
      tax = (54097 + ((person.income - 180000)*0.45));
    }
    
    output = person.name + "'s income is: $" + person.income + ", and her tax owed is: $" + tax;
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}