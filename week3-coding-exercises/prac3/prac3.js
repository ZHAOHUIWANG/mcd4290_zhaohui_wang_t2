question1()
question2()
question4()

function question1(){
    function objectToHTML(data){
        let Print="";

        for(let prop in data){
            Print += prop +":"+ data[prop]+"\n";
        }
        
        return Print;

    }

    var testObj = {
        number: 1,
        string: "abc",
        array: [5, 4, 3, 2, 1],
        boolean: true
    };
    
    let outPutArea1 = document.getElementById("outputArea1");
    
    outPutArea1.innerText = objectToHTML(testObj);
}

function question2(){
    var outputAreaRef = document.getElementById("outputArea3");
    var output = "";
    
    function flexible(fOperation, operand1, operand2) {
        var result = fOperation(operand1, operand2);
        
        return result;
    }
    
    function add(num1, num2){
        return num1 + num2;
    }
    
    function times(num1, num2){
        return num1 * num2;
    }
    
    output += flexible(add,3, 5) + "<br/>"; 
    output += flexible(times,3, 5) + "<br/>";
    
    outputAreaRef.innerHTML = output;
}

//q3
/*
pseudocode
Create a function named 'extremeValues' that have a array parameter

define two element of MIN and MAX

create a for statement to iterate through the array. start from 0th to find min.
  if the number of array less than 0th of array, the number will be new min
  if the number of array more than 0th of array, the number will be new max.
  
print result like:
max
min
*/

function question4(){
    var values = [6, 8, 2, 3, 12, 9, 7]
    
    extremeValues(values)
    
    function extremeValues(Array){
        let min = Array[0]
        let max = Array[0]
        
        for(let i=0; i < Array.length; i++){
            if(Array[i] < min){
                min = Array[i]
            }
            if (Array[i] > max){
                max = Array[i]
            }

        }
        return result = [max,min]
    }
    
    let outPutArea4 = document.getElementById("outputArea4");
    
    outPutArea4.innerText += result[0]+'\n'
    outPutArea4.innerText += result[1]
}









